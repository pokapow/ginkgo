SENTRY_DSN=https://306345cb87ee4e1cbbe9023fb4afc5fc@sentry.comunes.org/6

# Card customization
CARD_COLOR_LEFT=0xFF05112B
CARD_COLOR_RIGHT=0xFF085476
# Empty for default
CARD_COLOR_TEXT=Ğ1 Wallet

# Nodes space-separated
# The duniter nodes are only used at boot time, later it tries to calculate periodically the nodes
# that are available with the less latency
DUNITER_NODES=https://g1.duniter.fr https://g1.le-sou.org https://g1.cgeek.fr https://g1.monnaielibreoccitanie.org https://g1.duniter.fr https://g1.le-sou.org https://g1.cgeek.fr
CESIUM_PLUS_NODES=https://g1.data.le-sou.org https://g1.data.e-is.pro https://g1.data.presler.fr https://g1.data.mithril.re
GVA_NODES=https://g1v1.p2p.legal/gva
# not updated: https://g1.data.adn.life

